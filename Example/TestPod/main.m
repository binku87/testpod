//
//  main.m
//  TestPod
//
//  Created by binku on 10/17/2014.
//  Copyright (c) 2014 binku. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TAppDelegate class]));
    }
}
