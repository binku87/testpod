//
//  TAppDelegate.h
//  TestPod
//
//  Created by CocoaPods on 10/17/2014.
//  Copyright (c) 2014 binku. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
